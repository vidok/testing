class MyClass
  def self.perform(params)
    new(params).perform
  end

  private
  attr_reader :params

  def initialize(params = {})
    @params = params
  end

  def condition_met?(data)
    data.kind_of?(String) && data == "OMG"
  end

  def fix_data(data)
    data = "OMG"
  end

  def perform
    DataReader.new(params[:source]).each do |chunk, total, current|
      processor = Proc.new { |data|
        return if condition_met?(data)
        fix_data(data)
      }

      puts "Processing #{current}/#{total}"
      processor.call(chunk)
    end
  end
end
