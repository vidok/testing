$(document).ready(function() {
  Notification = (function() {
    Notification.prototype.show = function(message) {
    alert(message);
  }
  })();

  handler = function() {
    var HandlerClass = function() { }

    HandlerClass.prototype = {
       showNotice: function() {
         Notification.show("Clicked");
       },
       handler: function() {
         $(this.el).toggleClass('activated'); this.showNotice();
      },
      install: function() {
        this.el = $(this);
        $(this).on('click', this.handler) }
      }
      new HandlerClass()
    };

["a", "h1", "span.as-link"].forEach(function(type) { $(type).each(handler().install.bind(this))
}); });
